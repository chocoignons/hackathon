﻿using System;
using System.Xml.Linq;
using UnityEngine;
using Mapbox.Unity.MeshGeneration.Factories;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.MeshGeneration.Factories.TerrainStrategies;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Unity;
using Random = System.Random;

public class GamePlay : MonoBehaviour
{
    [SerializeField] AbstractMap _map;
    [SerializeField] GameObject _population;

    private const float Tau = 16f;
    private const float Alpha = 40;

    private const float max = 500;

    private const float SpawnTau = 1;
    private const float SpawnRate = 22026;
    private const int InitialChildCount = 64;
    private long T = 10;
    private float SpawnValue = 0;
    private int populations = 0;

    void SpawnPopulationAt(float lat, float lon)
    {
        Vector2d location = new Vector2d(lat, lon);
        var earthRadius = ((IGlobeTerrainLayer) _map.Terrain).EarthRadius;

        var instance = Instantiate(_population);
        populations++;
        instance.transform.position = Conversions.GeoToWorldGlobePosition(location, earthRadius);
        instance.transform.localScale = new Vector3(Alpha, Alpha, Alpha);
        instance.transform.SetParent(transform);

        Debug.Log("Spawning population #" + populations + " at [lat=" + lat + ";lon=" + lon + "]");
    }

    void SpawnPopulationAtRandom()
    {
        Random rng = new Random();
        float lat = rng.Next(0, 360) - 180;
        float lon = rng.Next(0, 360) - 180;
        SpawnPopulationAt(lat, lon);
    }

    void ExterminateAt(Vector3 point, float calamityRadius)
    {
        Collider[] PopsToBeEradicated = Physics.OverlapSphere(point, calamityRadius);
        foreach (Collider PopToBeEradicated in PopsToBeEradicated)
        {
            Destroy(PopToBeEradicated);
            populations--;
        }
    }

    void Update()
    {
        for (int i = InitialChildCount; i < transform.childCount; i++)
        {
            Transform childPopulation = transform.GetChild(i);
            if (childPopulation.transform.localScale.x < 500)
            {
                float dS = (float) Math.Exp(Time.deltaTime / Tau);
                childPopulation.localScale = childPopulation.localScale * dS;
//                childPopulation.gameObject.
            }
        }

        T += (long) Time.deltaTime;
        SpawnValue = T; //(float) Math.Log(T * SpawnRate);
        if (SpawnValue >= 10) // trigger a population spawn
        {
            if (populations < 10)
                SpawnPopulationAtRandom();
        }
    }
}